var fs = require('fs');
 
 
if (process.argv.length <= 2) {
    console.log("Usage: " + __filename + " path/to/directory");
    process.exit(-1);
}
 
var path = process.argv[2];
var title = process.argv[3];
fs.readdir(`./${path}`, function(err, items) {
    const newItems = items.map((i) => (`/imgs/${path}` + i))
    console.log(`${path.substring(0, path.length - 1)}: {
        title: '${title}',
        images: ${JSON.stringify(newItems)}
    }`);
});