import Vue from 'vue';
import VueRouter from 'vue-router';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'home',
    alias: '/index',
    component: () => import('@/components/Home.vue'),
  },
  {
    path: '/gallery/:album',
    name: 'album',
    alias: '/gallery/album',
    component: () => import('@/components/Album.vue'), 
  },
  {
    path: '/gallery',
    name: 'gallery',
    alias: '/gallery',
    component: () => import('@/components/Gallery.vue'),
  },
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
  scrollBehavior (to, from, savedPosition) {
    return { x: 0, y: 0 }
  }
});

export default router;
